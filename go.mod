module github.com/golobby/env/v2

go 1.11

require (
	github.com/golobby/cast v1.3.0
	github.com/stretchr/testify v1.7.0
)
